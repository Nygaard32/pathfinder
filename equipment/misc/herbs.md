# Herbs
I updated one of my friends list of all herb location with more information from  [Archive of Nethys - Herbs](https://aonprd.com/EquipmentMisc.aspx?Category=Herbs) section.

Name|Location|Cost (gp)|Gather (DC)|Yield (dose)|
-|-|-|-|-|
[Angelstep] | Warm Deserts | 25 | 18 | 1 |
[Angelstep] | Temperate Deserts | 25 | 18 | 1 |
[Black Amaranth] | Temperate Forests | 100 | 18 | 1 |
[Black Amaranth] | Temperate Plains | 100 | 18 | 1 |
[Black Amaranth] | Warm Forests | 100 | 18 | 1 |
[Black Amaranth] | Warm Plains | 100 | 18 | 1 |
[Bloody Mandrake] | Plains | 15 | 20 | 1d4 |
[Bloody Mandrake] | Swamps | 15 | 20 | 1d4 |
[Bone Reed] | Swamps | 75 | 24 | 1 |
[Cloud Puff] | Mountains | 100 | 18 | 1 |
[Desnas Star] | Forests | 5 | 13 | ? |
[Desnas Star] | Plains | 5 | 13 | ? |
[Dragon Rose] | Forests | 25 | 18 | 1 |
[Dragon Rose] | Mountains | 25 | 18 | 1 |
[Dream Lichen] | Material-Dream Plane Intersection | 2 000 | 30 | 1 |
[Fairy Cap] | Ley Lines | 250 | 20 | 1 |
[Goblinvine] | Forests | 30 | 16 | 1 |
[Goblinvine] | Mountains | 30 | 16 | 1 |
[Goblinvine] | Plains | 30 | 16 | 1 |
[Goblinvine] | Swamps | 30 | 16 | 1 |
[Leechwort] | Warm Forests | 3 | 16 | 1d4 |
[Leechwort] | Warm Swamps | 3 | 16 | 1d4 |
[Love-In-Idleness] | Forests | 150 | 20 | 1 |
[Merfolks Comb] | Underwater | 750 | 25 | 1 |
[Mimameith] | Forests | 600 | 25 | 1d4 (5 lbs. per dose) |
[Mimameith] | Mountains | 600 | 25 | 1d4 (5 lbs. per dose) |
[Moly] | Forests | 1 200 | 25 | 1 |
[Moly] | Plains | 1 200 | 25 | 1 |
[Nepenthe] | Warm Forests | 400 | 20 | 1 |
[Nepenthe] | Warm Swamps | 400 | 20 | 1 |
[Nethys Dagger] | Deserts | 50 | 18 | ? |
[Nightsage] | Jungles | 100 | 14 | 1 |
[Seeing Slime] | Underground | 160 | 19 | 1 |
[Winterbite] | Cold | 20 | 11 | 1d4 |


[Angelstep]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Angelstep
[Black Amaranth]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Black%20Amaranth
[Bloody Mandrake]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Bloody%20Mandrake
[Bone Reed]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Bone%20Reed
[Cloud Puff]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Cloud%20Puff
[Desnas Star]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Desna%27s%20star
[Dragon Rose]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Dragon%20Rose
[Dream Lichen]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Dream%20Lichen
[Fairy Cap]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Fairy%20Cap
[Goblinvine]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Goblinvine
[Leechwort]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Leechwort
[Love-In-Idleness]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Love-In-Idleness
[Merfolks Comb]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Merfolk%27s%20Comb
[Mimameith]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Mimameith
[Moly]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Moly
[Nepenthe]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Nepenthe
[Nethys Dagger]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Nethys%27s%20dagger
[Nightsage]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Nightsage
[Seeing Slime]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Seeing%20Slime
[Winterbite]: https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Winterbite