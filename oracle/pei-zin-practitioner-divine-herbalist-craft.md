# Pei Zin Practitioner/Divine Herbalist herbalism as alchemy
So I took the liberty of compiling a list of all items that probably are/should be possible to craft for a Pei zin Oracle by using [Archive of Nethys - Alchemical Remedies](https://www.aonprd.com/EquipmentMisc.aspx?Category=AlchemicalRemedies) section.

Probably/Should because rules as written in the Alchemy Manual do say this

> Practitioners of Pei Zin herbalism can use the Profession (herbalist) skill in place of a Craft (alchemy) check to perform enlightened alchemy, but only to create **alchemical remedies** that include at least one of the following plant-derived reagents in their alchemical recipes:

so depending on what your DM thinks it might not be okey to craft [alcohol](#alcohol) or [? (unkown)](#unkown) items this way even though they are listed on the [Archive of Nethys - Alchemical Remedies](https://www.aonprd.com/EquipmentMisc.aspx?Category=AlchemicalRemedies) section.

## Alchemical remedy
|Item|Price (gp)|Recipe|Craft|Time|Tool(s)|
|-|-|-|-|-|-|
[Antitoxin](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Antitoxin) | 50 | (7 gold + 17 myrrh + 23 urea)/ceration | 25 | 10 minutes | crucible |
[Blood-boiling pill](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Blood-boiling%20pill) | 75 | (30 ginger extract + 8 gold + 15 mugwort extract)/ceration | 25 | 10 minutes | crucible |
[Blood-chilling pill](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Blood-chilling%20pill) | 75 | (15 ginger extract + 40 myrrh + 30 mugwort extract)/filtration | 25 | 10 minutes | filter |
[Bloodgorge](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Bloodgorge) | 40 | (5 cold iron + 10 dew of lunary)/filtration | 25 | 10 minutes | filter |
[Essence of independence](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Essence%20of%20independence) | 80 | (8 dew of lunary + 10 gold + 22 spirit of wine)/sublimation | 25 | 1 day | retort |
[Eye drops of the unseen master](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Eye%20drops%20of%20the%20unseen%20master) | 150 | (20 dew of lunary + 20 saltpeter + 80 spirit of wine)/distillation | 25 | 1 day | retort |
[Insight leaves](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Insight%20leaves) | 50 | (10 dew of lunary + 5 mugwort extract + 20 spirit of wine)/fermentation | 25 | 1 day | heat source |
[Loyalty transfusion](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Loyalty%20transfusion) | 625 | (200 quicksilver + 150 realgar + 200 spirit of wine)/distillation | 30 | 1 day | retort |
[Semblance transfusion](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Semblance%20transfusion) | 300 | (110 magnesium + 200 silver + 100 spirit of wine)/distillation | 25 | 1 day | retort |
[Surge syrup](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Surge%20syrup) | 80 | (12 dew of lunary + 40 spirit of wine + 40 urea)/congelation | 25 | 10 minutes | alchemist's lab |
[Tea of transference](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Tea%20of%20transference) | 40 | (5 gold + 5 realgar + 15 spirit of wine)/sublimation | 20 | 1 day | retort |
[Vapors of easy breath](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Vapors%20of%20easy%20breath) | 75 | (60 myrrh + 50 salt + 60 ginger extract)/congelation | 25 | 10 minutes | alchemist's lab |
[Vivifying moxibustion needles](https://aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Vivifying%20moxibustion%20needles) | 85 | (20 phosphorus + 60 ginger extract + 30 mugwort extract)/filtration | 25 | 10 minutes | filter |

## Alcohol
|Item|Price (gp)|Recipe|Craft|Time|Tool(s)|
|-|-|-|-|-|-|
[Boulderhead bock](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Boulderhead%20bock) | 25 | (4 gold + 15 spirit of wine)/fermentation | 19 | 1 week | brewer's kit |
[Icecap ale](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Icecap%20ale) | 40 | (50 spirit of wine + 40 urea)/distillation | 20 | 1 week | brewer's kit |
[Longbeard lambic](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Longbeard%20lambic) | 20 | (3 gold + 5 quicksilver + 4 spirit of wine)/fermentation|18 | 1 week | brewer's kit |
[Wyrm's breath bitter](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Wyrm%27s%20breath%20bitter) | 30 | (2 black powder + 10 brimstone + 20 spirit of wine)/digestion | 21 | 1 week | brewer's kit |

## Unknown
|Item|Price (gp)|Recipe|Craft|Time|Tool(s)|
|-|-|-|-|-|-|
[Curative myrrh](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Curative%20myrrh) | 50 | ? | (Alchemy) DC 20 | ? | ? |
[Gildea myrrh](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Gildea%20myrrh) | 50 | ? | ? | ? | ? |
